package im.zhaojun.zfile.module.storage_search.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文件搜索模式枚举
 * 不知道FileTypeEnum的枚举用处，因为前端有三个搜索选项显然与该枚举不匹配，不知是否是开源版所以没更新
 * @author: zw
 * @date: 2024/7/30 15:34
 */
@Getter
@AllArgsConstructor
public enum FileSearchModeEnum {

    SEARCH_CURRENT_FOLDER("search_current_folder"),

    SEARCH_CURRENT_AND_CHILD("search_current_and_child"),

    SEARCH_ALL("search_all");

    @EnumValue
    @JsonValue
    private final String value;
}
