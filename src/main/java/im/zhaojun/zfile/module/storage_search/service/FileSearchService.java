package im.zhaojun.zfile.module.storage_search.service;

import im.zhaojun.zfile.module.storage.model.result.FileItemResult;
import im.zhaojun.zfile.module.storage_search.model.request.FileSearchRequest;

import java.util.List;

/**
 * 文件搜索接口
 * @author: zw
 * @date: 2024/7/30 16:17
 */
public interface FileSearchService {
    /**
     *
     * @param fileSearchRequest
     * @return
     */
    List<FileItemResult> search(FileSearchRequest fileSearchRequest) throws Exception;

}
