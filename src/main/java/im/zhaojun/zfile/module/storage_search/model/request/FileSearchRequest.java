package im.zhaojun.zfile.module.storage_search.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: zw
 * @date: 2024/7/30 14:55
 */
@Data
@ApiModel(description = "文件查询请求类")
public class FileSearchRequest {

    @ApiModelProperty(value = "存储源 key", required = true, example = "local")
    @NotBlank(message = "存储源 key 不能为空")
    private String storageKey;

    @ApiModelProperty(value = "请求路径", example = "/")
    private String path;

    @ApiModelProperty(value = "搜索词", example = "电子表格")
    private String searchKeyword;

    /**
     * search_current_folder  search_current_and_child  search_all
     */
    @ApiModelProperty(value = "搜索模式", example = "search_current_and_child")
    private String searchMode;

}
