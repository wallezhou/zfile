package im.zhaojun.zfile.module.storage_search.controller;

/**
 * 文件查询
 * @author: zw
 * @date: 2024/7/30 17:09
 */

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import im.zhaojun.zfile.core.util.AjaxJson;
import im.zhaojun.zfile.module.storage.model.result.FileItemResult;
import im.zhaojun.zfile.module.storage_search.model.request.FileSearchRequest;
import im.zhaojun.zfile.module.storage_search.service.FileSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@Api(tags = "文件查询")
@Slf4j
@RequestMapping("/api/storage")
@RestController
public class FileSearchController {

    @Autowired
    private ApplicationContext context;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "搜索", notes = "搜索存储源下文件")
    @PostMapping("/search")
    public AjaxJson<List<FileItemResult>> search(@Valid @RequestBody FileSearchRequest fileSearchRequest) throws Exception {
        FileSearchService instance = context.getBean(FileSearchService.class);
        List<FileItemResult> fileItemList = instance.search(fileSearchRequest);
        return AjaxJson.getSuccessData(fileItemList);
    }
}
